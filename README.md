# final_service
Чтобы скопировать репозиторий к себе для работы, вам нужно .

## О работе сценария
Сценарий развёртывания deploy_app.yml применяется к работающим AWS ec2 instance с тэгом final_server, создаваемым в результате работы [сценария подготовки инфраструктуры](https://gitlab.com/skillbox132/final_infra.git). Ansible inventory-файл, динамически наполняется посредством плагина aws_ec2.
В процессе развёртывания приложения производится тестирование инфраструктуры, скачивание ннеобходимых docker-image, удаление сервиса nginx на порту 8080.
Приложение устанавливается на порт 8080, отвечает по 3 эндпоинтам:  
* /health - 200 ok
* /metrics - в формате метрик для prometheus, включая счётчик запросов в основной эндпоинт `skillbox_http_requests_total`
* / - основной эндпоинт, возвращающий часть запроса и генерирующий строчку лога.

## Как развернуть приложение в инфраструктуре AWS:  
1. Склонировать репозиторий на локальный компьютер, следуя [этой инструкции](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-on-github/duplicating-a-repository#mirroring-a-repository-in-another-location).
2. Внести путь к своему приватному ключу доступа к AWS ec2 instance, скорректировав переменную private_key_file в файле ansible/ansible.cfg репозитория ().
3. При необходимости указать требуемый docker-image, скорректировав переменную docker_image в файле ansible/roles/web-app/defaults/main.yml.
4. Из контекста директории ansible репозитория выполнить команду:
   ```shell
   ansible-playbook deploy_app.yaml
   ```
5. Проверить доступность приложения по адресу http://<>>YOURDOMAIN.XX>:8080